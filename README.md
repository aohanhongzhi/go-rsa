我的公钥加密

```shell
./go-rsa  -text=1113
```

解密

```shell
go-rsa -encrypt=lbOvhRo8d4iZ24cF6QMOtdjrRvqG/U3VOmH7P0XPkaaJcanBeqHjjIVWJRe2UIuYTcOQzP8slOvvg6fv/EeiMGrWTqSFrMmSJQBDlW3JhB34Kmo8i9ihT+XkKWRz/dG1Gpv2d2M07DGT9edPXkYYnJoKKnu+GAaeSiq57Qyhl7x+J1EFJvTo040w8nnf+tFkUIQbDIdaUKwlfmf9QoS+96VfHRjD23Smyr1iAQVPnyilTzwEdLO5YcDcVsVhrkNtA++hWkBRe+JbWbWYZxW5n+bBJZbmK2TNtPHgpfflJa8qU3seiwRRi4attsmZGz6DModRxJhK9UkJ0nq1191GeQ==
```

本代码，可以读取本地的.ssh，来加解密。

![输入图片说明](assets/image.png)


# 斯诺登推荐的加密软件

更加高级！如果涉及到绝密的东西，可以使用这个来加密保存。生成的证书也是可以加上密码的，这样别人拿不到密码，拿到公私钥也是没啥用的。

https://gnupg.org/

linux版本下载

https://download.gnupg.com/files/gnupg/gnupg-desktop-2.4.3.0-x86_64.AppImage

![alt text](assets/image-1.png)


下面是生成公私钥，恢复成asc文本文件就可以导入到软件使用了。没有密码。

![alt text](assets/image-2.png)


## 公钥

```
-----BEGIN PGP PUBLIC KEY BLOCK-----

mDMEZivF1RYJKwYBBAHaRw8BAQdAbV0XKHLEkCZizg2Qlz2GhQtqJN2F4cgFIZoj
BsgYaRK0BGVyaWOImQQTFgoAQRYhBKP7kI+IHE6zge1V84/QIPRaVz1XBQJmK8XV
AhsDBQkFovtrBQsJCAcCAiICBhUKCQgLAgQWAgMBAh4HAheAAAoJEI/QIPRaVz1X
xCEBAP3Hr/+8xY5sCJC3+PvytJzubGcejDT/HfifKm1zcGzfAP4y6uI6rVZOwCML
MnGpI9+FQcOyQ3KmdjVpKjF8jxNhA7g4BGYrxdUSCisGAQQBl1UBBQEBB0BV+89m
EssQo5tY7taU1IiuGq/snyUU2y4M+9FUhbmWLAMBCAeIfgQYFgoAJhYhBKP7kI+I
HE6zge1V84/QIPRaVz1XBQJmK8XVAhsMBQkFovtrAAoJEI/QIPRaVz1XIM8A/An5
2B9q9kOFT4yVlMtbfKmkGmsv6Lnm8XkTu9iHqqg6AP99BLy2+hwP/tkDFPC/jidV
lb3ZFafH31cQE6UnCEWPBw==
=UTWE
-----END PGP PUBLIC KEY BLOCK-----
```

![alt text](assets/image-3.png)

软件里也可以导入分享给其他用户。

![alt text](assets/image-4.png)

## 私钥

下面私钥经过rsa加密了，需要解密才能使用

```
OpjiFSabKCiaVvkgS9hZJA4-MubyIQAnm4PCJSeewpXgyoTsRDaFxwb5_Py4_FyrLTljMDeO0GnZt0LwGzfJyO60ltYIUzGd9U8XPjBKxexvY4kY1svNgdIGWiS61x1fP10F79XOXg_0vADdHXcRrnswyBlHFs4k-aPFhkREugkL1ea_SYj1_Xe0SnTTloVV9E9Tn00w-hFVAeuVH0z4ZNdGsLpJGuDgHNsKdsklBBuMW5MaOtluvXTEnAwpeZVk876LJcXqW_LRRmIDNPSr166L8L6xVgwyZ20qycissmGfNV3EWsJX0AW_4XRemRLQsTRRhBCLdAPVk6XCQ24-7UEu2oaW7_XIskqcOXPuORceUrMPgvpEdOr6-Ue1NE1uJkVm-z6406KtbT_PXtAfHhUm7jxpqnNtYqC-F_Grz4LujXsPcfD82gF0EHfstlb2wgvauvy73QnPu8n_myQ7zJpOmThHEivKN3Bi298Zhmy4Tczd1XWzCODjDXWgbaExJ6dVg17FVqk9Jqsmx5JhoGhrc7TCMolOjTIcjbaso8eMU22H6N3hlZb7xJbODCFcZ4yJOoA7ZVK_7LZ38jIGA43-VhinDEydkj5GInGZDkN2cRTK32p_uoGcdBTK7z287JZTXbp3pW5ihiTkgbhUchn45z3cplsdKIUwGQP5nHaaHZiz7fGw09xTCqDyvrUqffNI4e5GxwMj-_VqktHXLHsdVLd673nPqH8af0TLIAlcsH-WEqozBTfvL0iA5K6BIRNl5uBpPmtx1Dseag66Cxc0fN42ZwoeWjIkivTiDP8jlaEe64l8_PPlCCfqdSrmc7-3zMXo4tQN-z1CU34wHT4WYC3rk7FynN5VoV_30zB6IGQvi1asJTA36jxkRjScYdl31zUBjc_Fa2QT4jCImWFQPjZsic0rdH8cK7DSB1DXNOpLbvjqvIMD2Bds_othuGWl0MMh8A0tz9eREUGLuwaU5AILEaHXisgritjQV9i0MDCTh0UFVX6SyWjiuj6w
```


https://docs.github.com/zh/authentication/managing-commit-signature-verification/generating-a-new-gpg-key

# ssh-ed25519

`ssh-ed25519` 是一种用于 SSH（Secure Shell）协议的公钥加密格式。它使用的是 Ed25519 椭圆曲线加密算法，这是一种现代的、高效的、安全的公钥签名算法。

当你生成 SSH 密钥对时，通常会看到以下格式的输出：

```
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIL+w5xJ6/gBf8LxqWK1kFb+w5xJ6/gBf8LxqWK1kFb user@example.com
```

这个字符串包含以下几个部分：

1. `ssh-ed25519`：表示使用的公钥加密算法。
2. `AAAAC3NzaC1lZDI1NTE5AAAAIL+w5xJ6/gBf8LxqWK1kFb+w5xJ6/gBf8LxqWK1kFb`：这是实际的公钥部分，使用 Base64 编码。
3. `user@example.com`：这是注释部分，通常用于标识密钥的所有者。

Ed25519 算法相对于传统的 RSA 和 DSA 算法有以下优点：

- **安全性**：Ed25519 提供了 128 位的安全级别，比 RSA 2048 位更安全。
- **性能**：Ed25519 在签名和验证操作上比 RSA 和 DSA 更快。
- **密钥大小**：Ed25519 密钥更小，便于传输和存储。

因此，使用 `ssh-ed25519` 格式的密钥对可以提供更高的安全性和更好的性能。

# 其他开源加密项目

## VeraCrypt

下面这个主要是加密磁盘数据。

https://github.com/veracrypt/VeraCrypt
https://www.veracrypt.fr/



## age

利用公私钥加密文件

https://github.com/FiloSottile/age