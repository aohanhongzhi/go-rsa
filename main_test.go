package main

import (
	"fmt"
	"os"
	"testing"
)

func TestEncryptAndDecrypt(t *testing.T) {

	// 我自己的公钥，下面这个格式不可以调整，否则会出现decode错误
	publicKey := []byte(`-----BEGIN RSA PUBLIC KEY-----
MIIBCgKCAQEAsab/Egk9M0GHSZE4UF4BNxfNufRaFTxHF19BmKt3RVtfdIbb0xjv
Frgx7sYFJgBPsPvM00zcwIIWWIRBwZFqhvH9pLStTzIy8j2PJ/cIn2V1q4A1W8it
8g4F/WAhlHl6w6ESM0Ixz1dKjuiV73bjEajviMxuCm9DfpIuBpWGIgXagLRAak/c
pUK5s/4aqTbP+1TdYp/i7jPB+LAxr3lSpHPM7llgS/lsl9Vnw+FJOe+jiDLUimXO
L9t2forcSxhB3lfQ+2tTpE2Nuq/nBBrfeTjmi3KJC7FVKou2F/oCwpmU8ann44Oj
PgJeIRKCMsXpU3fcPth9H7gdSz52K5eNAwIDAQAB
-----END RSA PUBLIC KEY-----
`)

	//	publicKey = []byte(`-----BEGIN RSA PUBLIC KEY-----
	//MIIBigKCAYEAzv5ic7VzD3WXpyMKvcSf6LJitHEH1/7wB+PS86abk/fkTRbI7m4X
	//ObPhPa9EE658Q3UOJS9Urfq7BiOF76NPHYH21mpBmOFkpq8sjrtEpsE4SDNCElMq
	//d57SPgi9KfsYRQZFi0E6f0ph7F4plEE4hXmW8imkReEBq2gAEOzhkVDIMnoqRKRj
	//vTTzNGlmAgo4wmQ4q5t5/WLHj6g2gKlHXczzLhIkNaSofDsXu5nlZuyGl3T8wWRH
	//nc3IIVO3ihA6KhARqcXmoo4XDj0OOZt+RM1k1hbQvLvp+ehBvPWLzd9sf6xV9RMP
	//fx77mwIk3d2bAhirxjwLYTadikAGP4rgfcmAA8yyx8xKs97YVzUa3s3qMj6NfNXM
	//iPf59+DaiGFWMT1JguDdRvHkHLEQtAtkZ3FjHjaTft/KG+EysTj7i4hQwFZTNgmu
	//XHO2hDtxlX2vwiGki4evejsUIdNe4ckgf8HjZyjBGeGel2kHqcwSKAD/rc8Rbun/
	//zquIf+hTtqfHAgMBAAE=
	//-----END RSA PUBLIC KEY-----
	//`)

	// 秦时明月
	publicKey = []byte(`-----BEGIN RSA PUBLIC KEY-----
MIIBigKCAYEAwe+VtRCwuHC/s9Xp0AyV1vCbmU8MMzhN+qubKeHQX7LV1eQ+a94a
otxo1y9fY+BSHTqHMB1jCAjDHibPC9zMEKsQ//0dp62R3kwtCJl+AuKVPJsXsyTz
2NWP7MpC9schedwtwT7FVZWafjRACm59HFKKTrhr4OQI6Fo4rt7QP50PG4YRWDna
g/cYodKExQKtpmuK9VnnnJulsZcAcC9SK3SZSOVRGzoV58Qz+2dI9012wSyCgjEO
3mYkv0u1IEY+SQ5KtlYZhlYabpt3S/siTuHL7oMnfqQy7LUH3kqq5LtN3u188SFp
Jza3eEoClfwQgwyKRHFlq9b8/z7KL3lVHOhKUyMe0RubZtxbaUuo0VIWhv91UfB0
BK+eQ0fJ8jv054KRXwSMD784x0Xny0HQbGUo8s/q4MGCMjjXXPI9Ecg5f+GP9usk
FfJWcJesnPD4wk8KqMVLAEF8dJESZIG50Sh1JuI1D4ccp7JeyriZ1JB+krqVLDp2
1C1oOjcX0+xrAgMBAAE=
-----END RSA PUBLIC KEY-----`)

	var result []byte
	for _, v := range publicKey {
		if v != 9 { // 去掉byte为 9 的值
			result = append(result, v)
		}
	}
	publicKey = result
	t.Log("公钥信息", string(publicKey))

	dir, err2 := os.UserHomeDir()
	if err2 != nil {
		log.Fatal(err2)
	}
	var privateKeyPath = dir + "/.ssh/id_rsa"
	t.Log("读取私钥路径", privateKeyPath)
	privateKey, err2 := os.ReadFile(privateKeyPath)
	if err2 != nil {
		panic(err2)
	}

	xRsa, err := NewXRsa(publicKey, privateKey)
	if err != nil {
		log.Panic(err)
	}

	rawData := `待加密文本`

	encrypt, err := xRsa.PublicEncrypt(rawData)
	if err != nil {
		log.Println("加密错误", err2)
	} else {
		log.Println("加密后\n" + encrypt)
	}
	//	encrypt = `待解密文本`
	decrypt, err := xRsa.PrivateDecrypt(encrypt)
	if err != nil {
		log.Println("解密错误", err)
	} else {
		log.Println("解密后\n" + decrypt)
	}

}

// 生成公私钥
func TestCreateFile(t *testing.T) {

	// 将私钥写入文件
	privateKeyFile, err := os.Create("private_key.pem")
	if err != nil {
		fmt.Println("Error creating private key file:", err)
		return
	}
	defer privateKeyFile.Close()

	// 将公钥写入文件
	publicKeyFile, err := os.Create("public_key.pem")
	if err != nil {
		fmt.Println("Error creating public key file:", err)
		return
	}
	defer publicKeyFile.Close()

	CreateKeys(publicKeyFile, privateKeyFile, 3072)

}
