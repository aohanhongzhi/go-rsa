package rsa

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/x509"
	"encoding/base64"
	"encoding/pem"
	"fmt"
	"golang.org/x/crypto/ssh"
	"log"
	"os"
	"testing"
)

// RSA 秘钥生成和测试
func TestRSAEncrypt(t *testing.T) {
	// 生成 RSA 密钥对
	privateKey, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		fmt.Println("Error generating RSA key:", err)
		return
	}

	// 将私钥写入文件
	privateKeyFile, err := os.Create("private_key.pem")
	if err != nil {
		fmt.Println("Error creating private key file:", err)
		return
	}
	defer privateKeyFile.Close()

	err = writePrivateKeyToPEM(privateKeyFile, privateKey)
	if err != nil {
		fmt.Println("Error writing private key to file:", err)
		return
	}
	fmt.Println("Private key written to private_key.pem")

	// 将公钥写入文件
	publicKeyFile, err := os.Create("public_key.pem")
	if err != nil {
		fmt.Println("Error creating public key file:", err)
		return
	}
	defer publicKeyFile.Close()

	err = writePublicKeyToPEM(publicKeyFile, &privateKey.PublicKey)
	if err != nil {
		fmt.Println("Error writing public key to file:", err)
		return
	}
	fmt.Println("Public key written to public_key.pem")

	// 原始数据
	originalData := []byte("Hello, RSA!")

	// 使用公钥进行加密
	ciphertext, err := rsa.EncryptOAEP(
		sha256.New(),
		rand.Reader,
		&privateKey.PublicKey,
		originalData,
		nil,
	)
	if err != nil {
		fmt.Println("Error encrypting data:", err)
		return
	}

	fmt.Println("Encrypted data:", ciphertext)

	// Base64 编码加密后的数据
	encodedCiphertext := base64.StdEncoding.EncodeToString(ciphertext)
	fmt.Println("Encrypted data (Base64):", encodedCiphertext)

	// Base64 解码密文
	decodedCiphertext, err := base64.StdEncoding.DecodeString(encodedCiphertext)
	if err != nil {
		fmt.Println("Error decoding Base64:", err)
		return
	}

	// 使用私钥进行解密
	decryptedData, err := rsa.DecryptOAEP(
		sha256.New(),
		rand.Reader,
		privateKey,
		//ciphertext,
		decodedCiphertext,
		nil,
	)
	if err != nil {
		fmt.Println("Error decrypting data:", err)
		return
	}

	fmt.Println("Decrypted data:", string(decryptedData))
}

// 将私钥写入 PEM 文件
func writePrivateKeyToPEM(file *os.File, privateKey *rsa.PrivateKey) error {
	block := &pem.Block{
		Type:  "RSA PRIVATE KEY",
		Bytes: x509.MarshalPKCS1PrivateKey(privateKey),
	}
	return pem.Encode(file, block)
}

// 将公钥写入 PEM 文件
func writePublicKeyToPEM(file *os.File, publicKey *rsa.PublicKey) error {
	bytes := x509.MarshalPKCS1PublicKey(publicKey)

	block := &pem.Block{
		Type:  "RSA PUBLIC KEY",
		Bytes: bytes,
	}
	return pem.Encode(file, block)
}

// RSA 加解密
func TestRSAString(t *testing.T) {

	// 替换为你自己的公钥和私钥
	privateKeyPEM := `-----BEGIN RSA PRIVATE KEY-----
MIIEogIBAAKCAQEAxvOWQwczRkShj5kQFgGnNjFHaIunjJlnyR0mTDt3OI7VES6m
0A7ghNxwAnijVYLnd34pqsVD4XJurY/Uv1nrdEvEi78q07hNAlnClnnsMb7y+Hot
ckq4G91ymspnAV6NZotCpsY7q0LwsSNCDzwtiRTk5DcMTKXfj/88xYV3AigMYAM9
vC+oilYtUcA1ebosWpbA01hLbNJQ1pOc7jK6TIqqeQ7WRa9t/jIUnrpMXO70icK4
ihKsoTkV2v4ts4f2iIXfhf3RfNtkRvKNQLCkyt9gtPio8UDeiBZol+nWbQTZefun
Jhgno+BcuCier5CvGoS7OuDIeKc3XC5TgwLUvQIDAQABAoIBADdeTUBEPbndmdT7
9SUauq1zU3s9+pg/jUnESgdHOWvNHHEWuagHq+Zt7uJRLqu6ra5sty3mjnisvy6g
a8YBQWzxNTbrMgEQNqZj0qrMLba+fKI4ZnN7ZfejTqTTlY0lAlc6hIcjfjGwhInX
1SemSMnwva7hvxOJykkp8IDMHw3DF6nXNyuyQdLCNPo/ZueqZhSTs6jPbohkKzlp
XbgLiBXlWKmwfm+EGw4cqigVadiN2p4KhWYDXdIqnCAy7OoSOA4rTFSYMU2C5mag
7Y6EaFrwEHbtsir2OD9x0vFtG9ZlCR8liiL5bL9EjzfsDEOe/tWkmPm4sFFYkgu3
TWTvKmECgYEA8EN62xVGD1EkQvANlkkw1o4vLjBIvkfK3sKl4jYzMai/n77dz1oD
feHac19c8GVdaRSzBxBjNNOGxSFwf0GQEtjuyelZMNrk3kZfzhF9X2UcXhbm11oG
xBEUJJkwI25rBg18vXsprp2McKfhQ+C6u4i2Vp5oPImQlTgQtT7I+ckCgYEA0/ts
go6GtWg+JP0ugAsHRLeXq50Uh32dEsCPtyJCmGHN9BnbkzHfE5q2CAHvjsb+oQgn
t2tGHf6el0H3m8N1aQj+XgY91I4ymSAtZ8NHcUE03Qsnbr2xnaJ6FZMuknPNVIl/
o37H5/fsN+gXH/lquaJ18cPuLjrlzA/pyEIrPVUCgYAaKT7XXBmk0VEBeOIB2zZW
JmU8wkUpcufEGa22U/B/F8DFxCKi0UpQCqeeqGOWSXNCXd6ghkhHGEE0dc+qC5mE
c9qMYbw0daE2ErOb3IuqN3g/Rn8cM9FIyBNJaOS09vDwV8edWivPt5fzLFR6751l
jh1MkTMIZHI5zUEEezGnAQKBgCQVpH8E7AFtYUbComvBdNdJuVPJRXla9KYAiXZP
Kq7t1zU6QZpo3uRjbaEnUzQmw6l9zeT3KdRpR23QGOo294GNul8fCN9p5UsToycK
otqwkuMdxvSe7eE7izSo/UCI3ecKEOQv7K+cEGXj6CvVNUmsUg95ns3X0M5P1yfz
u5CRAoGADPbZUl3x0gEFecs5Z+G+9qa6PXCoKnSkF8IxnaRwX/ZEZWpyrdyWIGXp
YTMmK1I2/ZfKSvyOi+lFnfXr3zPfsVNRQwJ/fm+j2IsMzVfBJsYiAtmzmVC4G74F
jRJIpEaUjxQ1t7FI7jCbGKGmlN7g2yZMu4Y7LpENsF6D5+3pUK4=
-----END RSA PRIVATE KEY-----
`

	//( 加密一般是使用对方的公钥，这样对方就可以使用它自己的私钥解密)
	publicKeyPEM := `-----BEGIN RSA PUBLIC KEY-----
MIIBCgKCAQEAxvOWQwczRkShj5kQFgGnNjFHaIunjJlnyR0mTDt3OI7VES6m0A7g
hNxwAnijVYLnd34pqsVD4XJurY/Uv1nrdEvEi78q07hNAlnClnnsMb7y+Hotckq4
G91ymspnAV6NZotCpsY7q0LwsSNCDzwtiRTk5DcMTKXfj/88xYV3AigMYAM9vC+o
ilYtUcA1ebosWpbA01hLbNJQ1pOc7jK6TIqqeQ7WRa9t/jIUnrpMXO70icK4ihKs
oTkV2v4ts4f2iIXfhf3RfNtkRvKNQLCkyt9gtPio8UDeiBZol+nWbQTZefunJhgn
o+BcuCier5CvGoS7OuDIeKc3XC5TgwLUvQIDAQAB
-----END RSA PUBLIC KEY-----
`

	// 解析 PEM 格式的私钥
	privateKey, err := parseRSAPrivateKey(privateKeyPEM)
	if err != nil {
		fmt.Println("Error parsing private key:", err)
		return
	}

	// 解析 PEM 格式的公钥
	publicKey, err := parseRSAPublicKey(publicKeyPEM)
	if err != nil {
		fmt.Println("Error parsing public key:", err)
		return
	}

	// 原始数据
	originalData := []byte("Hello, RSA!中文，江南夜色下烟火绝。")

	// 使用公钥进行加密
	ciphertext, err := rsa.EncryptOAEP(
		sha256.New(),
		rand.Reader,
		publicKey,
		originalData,
		nil,
	)
	if err != nil {
		fmt.Println("Error encrypting data:", err)
		return
	}

	fmt.Println("Encrypted data:", ciphertext)

	// Base64 编码加密后的数据
	encodedCiphertext := base64.StdEncoding.EncodeToString(ciphertext)
	fmt.Println("Encrypted data (Base64):", encodedCiphertext)

	//encodedCiphertext = "NrggQN+fT5/5rbk1d+hX2YJWN5yb81BI7tsTcBykHiDHkJ8G/loCBTvEqDr46+cn8h/MRh+jPh4AYcNvw1Xzl3yyTjLh1FRbEev1NaL1c2oe4zKGYEPN4iFbCElQElBBvlTqta7fu3jR9HR+Fu3pMiOEV69+0AsPT7FjY3UrohE="
	// Base64 解码密文
	decodedCiphertext, err := base64.StdEncoding.DecodeString(encodedCiphertext)
	if err != nil {
		fmt.Println("Error decoding Base64:", err)
		return
	}

	// 使用私钥进行解密
	decryptedData, err := rsa.DecryptOAEP(
		sha256.New(),
		rand.Reader,
		privateKey,
		//ciphertext,
		decodedCiphertext,
		nil,
	)
	if err != nil {
		fmt.Println("Error decrypting data:", err)
		return
	}

	fmt.Println("Decrypted data:", string(decryptedData))
}

// RSA 加解密
func TestRSAFile(t *testing.T) {

	// 替换为你自己的公钥和私钥

	file, err2 := os.ReadFile("id_rsa_privite.pem")
	if err2 != nil {
		panic(err2)
	}
	// 解析 PEM 格式的私钥
	privateKey, err := parseRSAPrivateKeyBytes(file)
	if err != nil {
		fmt.Println("Error parsing private key:", err)
		return
	}

	publicKeyFile, err2 := os.ReadFile("id_rsa_public_pkcs1.pem")
	if err2 != nil {
		panic(err2)
	}
	// 解析 PEM 格式的公钥
	publicKey, err := parseRSAPublicKeyBytes(publicKeyFile)
	if err != nil {
		fmt.Println("Error parsing public key:", err)
		return
	}

	// 原始数据
	originalData := []byte("Hello, RSA!中文，江南夜色下烟火绝。")

	// 使用公钥进行加密
	ciphertext, err := rsa.EncryptOAEP(
		sha256.New(),
		rand.Reader,
		publicKey,
		originalData,
		nil,
	)
	if err != nil {
		fmt.Println("Error encrypting data:", err)
		return
	}

	fmt.Println("Encrypted data:", ciphertext)

	// Base64 编码加密后的数据
	encodedCiphertext := base64.StdEncoding.EncodeToString(ciphertext)
	fmt.Println("Encrypted data (Base64):", encodedCiphertext)

	//encodedCiphertext = "NrggQN+fT5/5rbk1d+hX2YJWN5yb81BI7tsTcBykHiDHkJ8G/loCBTvEqDr46+cn8h/MRh+jPh4AYcNvw1Xzl3yyTjLh1FRbEev1NaL1c2oe4zKGYEPN4iFbCElQElBBvlTqta7fu3jR9HR+Fu3pMiOEV69+0AsPT7FjY3UrohE="
	// Base64 解码密文
	decodedCiphertext, err := base64.StdEncoding.DecodeString(encodedCiphertext)
	if err != nil {
		fmt.Println("Error decoding Base64:", err)
		return
	}

	// 使用私钥进行解密
	decryptedData, err := rsa.DecryptOAEP(
		sha256.New(),
		rand.Reader,
		privateKey,
		//ciphertext,
		decodedCiphertext,
		nil,
	)
	if err != nil {
		fmt.Println("Error decrypting data:", err)
		return
	}

	fmt.Println("Decrypted data:", string(decryptedData))
}

// 辅助函数，解析 PEM 格式的私钥
func parseRSAPrivateKey(pemData string) (*rsa.PrivateKey, error) {
	return parseRSAPrivateKeyBytes([]byte(pemData))
}

func parseRSAPrivateKeyBytes(pemData []byte) (*rsa.PrivateKey, error) {
	block, _ := pem.Decode(pemData)
	if block == nil {
		return nil, fmt.Errorf("failed to parse PEM block containing the private key")
	}

	privateKey, err := x509.ParsePKCS1PrivateKey(block.Bytes)
	if err != nil {
		return nil, fmt.Errorf("failed to parse private key: %v", err)
	}

	return privateKey, nil
}

// 辅助函数，解析 PEM 格式的公钥
func parseRSAPublicKey(pemData string) (*rsa.PublicKey, error) {
	return parseRSAPublicKeyBytes([]byte(pemData))
}

// 辅助函数，解析 PEM 格式的公钥
func parseRSAPublicKeyBytes(pemData []byte) (*rsa.PublicKey, error) {
	block, _ := pem.Decode(pemData)
	if block == nil {
		return nil, fmt.Errorf("failed to parse PEM block containing the public key")
	}

	publicKey, err := x509.ParsePKCS1PublicKey(block.Bytes)
	if err != nil {
		return nil, fmt.Errorf("failed to parse public key: %v", err)
	}

	return publicKey, nil
}

// 直接读取.ssh目录下的公私钥加解密
func TestRSASSHFile(t *testing.T) {

	// 替换为你自己的公钥和私钥
	dir, err2 := os.UserHomeDir()
	if err2 != nil {
		log.Fatal(err2)
	}
	var privateKeyPath = dir + "/.ssh/id_rsa"

	file, err2 := os.ReadFile(privateKeyPath)
	if err2 != nil {
		panic(err2)
	}
	// 解析 PEM 格式的私钥
	privateKey, err := parseRSAPrivateKeyBytes(file)
	if err != nil {
		fmt.Println("Error parsing private key:", err)
		return
	}

	// 读取.ssh的公钥文件
	var publicKeyPath = dir + "/.ssh/id_rsa.pub"

	publicKeyBytes, err := os.ReadFile(publicKeyPath)
	if err != nil {
		log.Fatalf("Error reading public key file: %v", err)
	}

	// 解析公钥
	parsedKey, _, _, _, err := ssh.ParseAuthorizedKey(publicKeyBytes)
	if err != nil {
		log.Fatalf("Error parsing public key: %v", err)
	}

	rsaPublicKey, ok := parsedKey.(ssh.CryptoPublicKey).CryptoPublicKey().(*rsa.PublicKey)
	if !ok {
		log.Fatalf("Error converting to *rsa.PublicKey")
	}
	key := x509.MarshalPKCS1PublicKey(rsaPublicKey)
	// 将公钥编码为 PEM 格式
	pemBytes := pem.EncodeToMemory(&pem.Block{
		Type:  "RSA PUBLIC KEY",
		Bytes: key,
	})

	// 解析 PEM 格式的公钥
	publicKey, err := parseRSAPublicKeyBytes(pemBytes)
	if err != nil {
		fmt.Println("Error parsing public key:", err)
		return
	}

	// 原始数据
	originalData := []byte("Hello, RSA!中文，江南夜色下烟火绝。直接利用.ssh的公私钥加解密")

	// 使用公钥进行加密
	ciphertext, err := rsa.EncryptOAEP(
		sha256.New(),
		rand.Reader,
		publicKey,
		originalData,
		nil,
	)
	if err != nil {
		fmt.Println("Error encrypting data:", err)
		return
	}

	// Base64 编码加密后的数据
	encodedCiphertext := base64.StdEncoding.EncodeToString(ciphertext)
	fmt.Println("Encrypted data (Base64):", encodedCiphertext)

	//encodedCiphertext = "NrggQN+fT5/5rbk1d+hX2YJWN5yb81BI7tsTcBykHiDHkJ8G/loCBTvEqDr46+cn8h/MRh+jPh4AYcNvw1Xzl3yyTjLh1FRbEev1NaL1c2oe4zKGYEPN4iFbCElQElBBvlTqta7fu3jR9HR+Fu3pMiOEV69+0AsPT7FjY3UrohE="
	// Base64 解码密文
	decodedCiphertext, err := base64.StdEncoding.DecodeString(encodedCiphertext)
	if err != nil {
		fmt.Println("Error decoding Base64:", err)
		return
	}

	// 使用私钥进行解密
	decryptedData, err := rsa.DecryptOAEP(
		sha256.New(),
		rand.Reader,
		privateKey,
		//ciphertext,
		decodedCiphertext,
		nil,
	)
	if err != nil {
		fmt.Println("Error decrypting data:", err)
		return
	}

	fmt.Println("Decrypted data:", string(decryptedData))
}

// 读取.ssh的公钥文件，转成PKCS1的pem文件
func TestSSHPublicKeyPemFile(t *testing.T) {
	// 读取公钥文件
	dir, err2 := os.UserHomeDir()
	if err2 != nil {
		log.Fatal(err2)
	}
	var publicKeyPath = dir + "/.ssh/id_rsa.pub"

	publicKeyBytes, err := os.ReadFile(publicKeyPath)
	if err != nil {
		log.Fatalf("Error reading public key file: %v", err)
	}

	publicKeyBytes = []byte("ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDO/mJztXMPdZenIwq9xJ/osmK0cQfX/vAH49LzppuT9+RNFsjubhc5s+E9r0QTrnxDdQ4lL1St+rsGI4Xvo08dgfbWakGY4WSmryyOu0SmwThIM0ISUyp3ntI+CL0p+xhFBkWLQTp/SmHsXimUQTiFeZbyKaRF4QGraAAQ7OGRUMgyeipEpGO9NPM0aWYCCjjCZDirm3n9YsePqDaAqUddzPMuEiQ1pKh8Oxe7meVm7IaXdPzBZEedzcghU7eKEDoqEBGpxeaijhcOPQ45m35EzWTWFtC8u+n56EG89YvN32x/rFX1Ew9/HvubAiTd3ZsCGKvGPAthNp2KQAY/iuB9yYADzLLHzEqz3thXNRrezeoyPo181cyI9/n34NqIYVYxPUmC4N1G8eQcsRC0C2RncWMeNpN+38ob4TKxOPuLiFDAVlM2Ca5cc7aEO3GVfa/CIaSLh696OxQh017hySB/weNnKMEZ4Z6XaQepzBIoAP+tzxFu6f/Oq4h/6FO2p8c= zoulh9187@outlook.com")

	// 解析公钥
	parsedKey, _, _, _, err := ssh.ParseAuthorizedKey(publicKeyBytes)
	if err != nil {
		log.Fatalf("Error parsing public key: %v", err)
	}

	rsaPublicKey, ok := parsedKey.(ssh.CryptoPublicKey).CryptoPublicKey().(*rsa.PublicKey)
	if !ok {
		log.Fatalf("Error converting to *rsa.PublicKey")
	}
	key := x509.MarshalPKCS1PublicKey(rsaPublicKey)
	// 将公钥编码为 PEM 格式
	pemBytes := pem.EncodeToMemory(&pem.Block{
		Type:  "RSA PUBLIC KEY",
		Bytes: key,
	})

	// 将 PEM 格式的公钥写入文件
	pemFilePath := "id_rsa_public_pkcs1.pem"
	pemFile, err := os.Create(pemFilePath)
	if err != nil {
		log.Fatalf("Error creating PEM file: %v", err)
	}
	defer pemFile.Close()

	_, err = pemFile.Write(pemBytes)
	if err != nil {
		log.Fatalf("Error writing PEM file: %v", err)
	}

	fmt.Printf("Public key in PKCS#1 format has been written to %s\n", pemFilePath)

}


func TestName(t *testing.T) {
	a := []byte(`a		q`)
	println(a)
	for _, cell := range a {
		println(cell)
	}
}