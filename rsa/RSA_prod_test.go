package rsa

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"encoding/base64"
	"fmt"
	"log"
	"os"
	"testing"
)

// 使用对方的公钥加密
func TestEncypt(t *testing.T) {
	//( 加密一般是使用对方的公钥，这样对方就可以使用它自己的私钥解密)
	publicKeyPEM := `-----BEGIN RSA PUBLIC KEY-----
MIIBCgKCAQEAsab/Egk9M0GHSZE4UF4BNxfNufRaFTxHF19BmKt3RVtfdIbb0xjv
Frgx7sYFJgBPsPvM00zcwIIWWIRBwZFqhvH9pLStTzIy8j2PJ/cIn2V1q4A1W8it
8g4F/WAhlHl6w6ESM0Ixz1dKjuiV73bjEajviMxuCm9DfpIuBpWGIgXagLRAak/c
pUK5s/4aqTbP+1TdYp/i7jPB+LAxr3lSpHPM7llgS/lsl9Vnw+FJOe+jiDLUimXO
L9t2forcSxhB3lfQ+2tTpE2Nuq/nBBrfeTjmi3KJC7FVKou2F/oCwpmU8ann44Oj
PgJeIRKCMsXpU3fcPth9H7gdSz52K5eNAwIDAQAB
-----END RSA PUBLIC KEY-----
`
	// 解析 PEM 格式的公钥
	publicKey, err := parseRSAPublicKey(publicKeyPEM)
	if err != nil {
		fmt.Println("Error parsing public key:", err)
		return
	}
	// 原始数据
	originalData := []byte("Hello, RSA!中文，江南夜色下烟火绝。")

	// 使用公钥进行加密
	ciphertext, err := rsa.EncryptOAEP(
		sha256.New(),
		rand.Reader,
		publicKey,
		originalData,
		nil,
	)
	if err != nil {
		fmt.Println("Error encrypting data:", err)
		return
	}

	// Base64 编码加密后的数据
	encodedCiphertext := base64.StdEncoding.EncodeToString(ciphertext)
	fmt.Println("Encrypted data (Base64):\n", encodedCiphertext)
}

// 使用自己的私钥，解密对方加密的信息
func TestDecypt(t *testing.T) {
	// 替换为你自己的公钥和私钥
	dir, err2 := os.UserHomeDir()
	if err2 != nil {
		log.Fatal(err2)
	}
	var privateKeyPath = dir + "/.ssh/id_rsa"

	file, err2 := os.ReadFile(privateKeyPath)
	if err2 != nil {
		panic(err2)
	}
	// 解析 PEM 格式的私钥
	privateKey, err := parseRSAPrivateKeyBytes(file)
	if err != nil {
		fmt.Println("Error parsing private key:", err)
		return
	}

	encodedCiphertext := "待解密的密文"
	encodedCiphertext = "ZrjCevKVpSPHUj0z9byPIIL6AAXHpCiZz/InPX1OkPENaNdnfCQp6S7tdQQ98PBRYO+003AUsCkHr7ycauUuT78D222jMtqrrBFIunY3xb/xG899V6NGL1Dh51yDCX98FxIwkuxRreqL11iz+KaZaYxUMG5wXvWMu8fe0C0d1qEUbHnPqG8xGTwkGcjbEZ6q/OnGjVlwnSBO6+xf1I7u5waA9H/BcWuSUcDG0cuITihfAo28ozKh0aOCDGaiz4i19A7dKlTGwB0Hm3OzqKlBTHK6+6UyotNzfAcfy8cecEUMKR48z1SdW2Ubbszb8jgtTMtmO/5Iya8gCQSYVEYQFw=="

	decodedCiphertext, err := base64.StdEncoding.DecodeString(encodedCiphertext)
	if err != nil {
		fmt.Println("Error decoding Base64:", err)
		return
	}

	// 使用私钥进行解密
	decryptedData, err := rsa.DecryptOAEP(
		sha256.New(),
		rand.Reader,
		privateKey,
		//ciphertext,
		decodedCiphertext,
		nil,
	)
	if err != nil {
		fmt.Println("Error decrypting data:", err)
		return
	}

	fmt.Println("Decrypted data:", string(decryptedData))
}
